//
//  main.m
//  AdsOnMobile
//
//  Created by Alexey Malafeyev on 5/3/13.
//  Copyright (c) 2013 Alexey Malafeyev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
