//
//  DetailViewController.h
//  AdsOnMobile
//
//  Created by Alexey Malafeyev on 5/3/13.
//  Copyright (c) 2013 Alexey Malafeyev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
